.. _debian_details:

########
Download
########

.. image:: images/debian.png
    :align: left

`Install FEniCS <apt://fenics>`__

#########################
Installation instructions
#########################

.. warning::

    The FEniCS packages in Debian is currently not up-to-date. To
    get the latest FEniCS release on Debian, we currently recommend
    to :ref:`install using Dorsal <installation_using_dorsal>`.

FEniCS is included as part of Debian GNU/Linux. To install, simply click
on the link above or run the following command in a terminal::

    sudo apt-get install fenics
