.. _support:

#######
Support
#######

This page describes how to get in contact to solve problems you might
encounter when using FEniCS. Help is available both from FEniCS
developers and other FEniCS users.

Depending on the type of question, comment or issue you might have,
you will need to address it to a specific forum or mailing
list. Please read on below to find out.


.. _help_answers:

Asking questions
================

User questions should be posted on `scicomp.stackexchange
<http://scicomp.stackexchange.com/>`__, which is a community-driven
forum for questions and answers regarding scientific computing.

Questions suitable for scicomp.stackexchange include questions like:

#. *How do I set boundary conditions in FEniCS?*
#. *Is P2/P0 a stable element for Stokes?*
#. *What is the best way to post-process my solution?*

Questions **not** suitable for scicomp.stackexchange include questions
like:

#. *Periodic boundary conditions give wrong results for BDM3 elements in parallel* (this should be reported as a bug/issue, see below)
#. *Can't find Boost when configuring on Windows 3.11* (this should be reported to the fenics-support mailing list, see below).

Before running away and posting on scicomp.stackexchange, please note
the following:

* Search the forum to check that your question has not been answered before.

* Search the `FEniCS user questions on Launchpad <http://answers.launchpad.net/fenics-project/>`__ to check that your question has not been answered before.

* Formulate your question well. If we cannot understand your question,
  we probably cannot help you.

* Post complete, but *minimal* code examples. No one wants to read
  through your complex application code.

* Consult the `guidelines <http://scicomp.stackexchange.com/faq>`__ for posting
  to scicomp.stackexchange.

* Please help out in answering questions from other users.

* Tag your question using the `FEniCS tag <http://scicomp.stackexchange.com/questions/tagged/fenics>`__.

Reporting bugs
==============

If you encounter something you suspect is a bug, please file a bug report
using the Bitbucket issue tracking system. In particular, for
reporting bugs in DOLFIN, visit the `DOLFIN issues page
<https://bitbucket.org/fenics-project/dolfin/issues>`_. As above, post
complete but *minimal* code examples.

Mailing lists
=============

.. _help_mailinglists:

The FEniCS mailing lists cover everything that does not fit into any of
the above categories (user questions or bug reports).

The development of FEniCS is discussed on the `main FEniCS mailing
list <http://fenicsproject.org/mailman/listinfo/fenics>`__. This is
also the place for announcements of workshops, releases, tutorials,
papers. Everyone is invited to subscribe to the mailing list and take
part in discussions.

Questions regarding the installation of FEniCS on specific
architectures, packaging and other practical questions can be posted
on the `FEniCS support mailing list
<http://fenicsproject.org/mailman/listinfo/fenics-support>`__. Users
are encouraged to subscribe and help out other users with installation
issues.

Contributing
============

We encourage users to take active part in developing and documenting
FEniCS. As an active developer, it is easy to influence the direction
and focus of the project. The step from user to developer is just a
patch away! For information about how to contribute code to the FEniCS
Project, visit the page :ref:`developers`.
